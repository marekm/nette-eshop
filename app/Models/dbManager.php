<?php
namespace App\Models;
use App\Forms;
use Nette;
 
final class dbManager {
    private $database;

    public function __construct(
        Nette\Database\Explorer $database,
        Forms\NovyPrispevekFactory $prispevekFactory
    )
	{
		$this->database = $database;
        $this->prispevekFactory = $prispevekFactory;
	}

    public function zobrazPrispevkyDB() {
        $prispevek = $this->database->table('dila')->select('*')->fetchAll();
        bdump($prispevek);
        return $prispevek;
    }

    public function vlozPrispevekDB($data) {
        bdump($data);
        $this->database->table('dila')->insert([
            'nazev' => $data->nazev,
            'cena' => $data->cena,
            'img' => $data->img,
            'autor_id' => $data->autor_id,
            'druh_id' => $data->druh_id
        ]);
        //$this->database->table('dila')->insert($data);
    }

}