<?php
namespace App\Forms;

use DateTime;
use Nette\Application\UI\Form;
 
final class NovyPrispevekFactory {
    function create(): Form {
        $form = new Form;
        
        $form->addText('nazev', 'Název díla:')->setRequired("Vypln");

        $form->addInteger('druh_id', 'Druh:')->setRequired("Vypln");    
        
        $form->addInteger('autor_id', 'Autor:')->setRequired("Vypln"); 

        $form->addInteger('cena', 'Cena položky:')->setRequired("Vypln");

        $form->addText('img', 'Vlož fotku:');

        //$form->addPassword('password', 'Heslo:');
        //$form->addPassword('passwordRepeat', 'Heslo znovu:');

        $form->addSubmit('send', 'Registrovat');
        //$form->onSuccess[] = [$this, 'formSucceeded']; //nutno dát tam, kde se bude používat, a vytvořit metodu
 
        return $form;
    }

    public function formSucceeded(Form $form, $data):void { // aktuálně se nevyužívá, protože na HomepagePresenteru onSuccess volám metodu na prezenteru vlozPrispevek()

       /* bdump($data);
        $data['autor'] = 1;
        bdump($data);*/
    }
 
}


 
?>