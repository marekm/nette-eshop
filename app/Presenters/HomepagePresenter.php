<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms;
use App\Models;
use Nette;
use Nette\Application\UI\Form;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private $db;
    private $prispevek;
    private $prispevekFactory;

    public function __construct(
        Forms\NovyPrispevekFactory $prispevekFactory,
        Models\dbManager $db
    )
	{
		$this->db = $db;
        $this->prispevekFactory = $prispevekFactory;
	}


    function renderDefault() {
        $this->ukazPrispevky();
        //bdump($prispevek);

        //n:href="Homepage:prispevek, $prispevek->id (volani renderPrispevek metody v homepagepresenteru s parametrem)" v homepagePresenter - renderPrispevek metoda napevno spjatá s tepmplate prispevek.latte
        //$this->template->prispevek = $this->prispevek->nazev;
    }

    public function ukazPrispevky() {
        $this->template->pozdrav = "Vítej";

        $this->template->dila = $this->db->zobrazPrispevkyDB();
    }

    public function createComponentNovyPrispevek(): Form {
        $form = $this->prispevekFactory->create();
        $form->onSuccess[] = [$this, 'vlozPrispevek'];//vola metodu v tomto souboru
        return $form;
    }

    public function vlozPrispevek(Form $form, $data):void {
        //$data['autor'] = 1;
        //($data);

        $this->db->vlozPrispevekDB($data);
    }
}
