<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms;
use App\Models;
use Nette;
use Nette\Application\UI\Form;

final class NovyClanekPresenter extends Nette\Application\UI\Presenter
{
    private $db;
    private $prispevek;
    private $prispevekFactory;

    public function __construct(
        Forms\NovyPrispevekFactory $prispevekFactory,
        Models\dbManager $db
    )
	{
		$this->db = $db;
        $this->prispevekFactory = $prispevekFactory;
	}


    function renderDefault() {

    }

    public function createComponentNovyPrispevek(): Form {
        $form = $this->prispevekFactory->create();
        $form->onSuccess[] = [$this, 'vlozPrispevek'];//vola metodu v tomto souboru
        return $form;
    }

    public function vlozPrispevek(Form $form, $data):void {
        //$data['autor'] = 1;
        //($data);
        $this->db->vlozPrispevekDB($data);
        $this->redirect('NovyClanek:');
    }
}
