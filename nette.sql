-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vytvořeno: Úte 12. dub 2022, 18:32
-- Verze serveru: 5.7.11
-- Verze PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `nette`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `author`
--

CREATE TABLE `author` (
  `id` int(11) NOT NULL,
  `dilo` int(11) DEFAULT NULL,
  `zarazeni` varchar(255) NOT NULL,
  `stoleti` int(11) NOT NULL,
  `puvod_id` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabulky `dila`
--

CREATE TABLE `dila` (
  `id` int(11) NOT NULL,
  `autor_id` varchar(255) NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `cena` int(11) NOT NULL,
  `druh_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `dila`
--

INSERT INTO `dila` (`id`, `autor_id`, `nazev`, `img`, `cena`, `druh_id`) VALUES
(2, 'Karel', 'Evzen opet zasahuje', '', 665, 'Zde'),
(3, 'Hynek', 'Evzen opet zasahuje', '', 665, 'muze'),
(4, 'Honza', 'Evzen opet zasahuje', '', 665, 'byt'),
(5, 'Karel', 'Evzen opet zasahuje', '', 665, 'text'),
(10, 'Václav', 'Uz ne evzen', '', 552, '2');

-- --------------------------------------------------------

--
-- Struktura tabulky `druh`
--

CREATE TABLE `druh` (
  `id` int(11) NOT NULL,
  `druh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `druh`
--

INSERT INTO `druh` (`id`, `druh`) VALUES
(1, 'proza'),
(2, 'poezie');

-- --------------------------------------------------------

--
-- Struktura tabulky `puvod`
--

CREATE TABLE `puvod` (
  `id` int(11) NOT NULL,
  `zeme` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabulky `ucty`
--

CREATE TABLE `ucty` (
  `id` int(11) NOT NULL,
  `jmeno` varchar(255) NOT NULL,
  `heslo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `dila`
--
ALTER TABLE `dila`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `druh`
--
ALTER TABLE `druh`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `puvod`
--
ALTER TABLE `puvod`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `ucty`
--
ALTER TABLE `ucty`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `author`
--
ALTER TABLE `author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `dila`
--
ALTER TABLE `dila`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pro tabulku `druh`
--
ALTER TABLE `druh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pro tabulku `puvod`
--
ALTER TABLE `puvod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `ucty`
--
ALTER TABLE `ucty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
